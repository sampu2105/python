import snowflake.connector
import pandas as pd
from snowflake.connector.pandas_tools import write_pandas
conn=snowflake.connector.connect(
            user='*****',
            password='*****',
            account='mo04822.ap-southeast-1',
            database = 'PYTHON',
            schema = 'CSV'
        )
ctx= conn.cursor()
df = pd.read_csv("~/Desktop/Sample-Spreadsheet-5000-rows.csv",encoding= 'unicode_escape',delimiter =",")
ctx.execute("CREATE OR REPLACE TABLE DEMO (ID NUMBER,BOOK_NAME VARCHAR(),NAME VARCHAR(),ORDER_NO NUMBER,STAMP1 NUMBER,STAMP2 NUMBER,STAMP3 NUMBER,DESCRIPTION VARCHAR(),INFO VARCHAR(),NUM NUMBER)")
success, nchunks, nrows, _ = write_pandas(conn,df,'DEMO')

